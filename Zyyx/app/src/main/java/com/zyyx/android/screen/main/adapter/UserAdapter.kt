package com.zyyx.android.screen.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.ssmedia.mecung.base.BaseViewModel
import com.zyyx.android.BR
import com.zyyx.android.R
import com.zyyx.android.utils.State

class UserAdapter(private var viewModel: BaseViewModel) : RecyclerView.Adapter<UserAdapter.ViewHolder>(){
    val NORMAL_STATE = 0
    val SEARCH_STATE = 1
    private var mSize = 0
    private lateinit var mState : State

    fun setState(state : State){
        mState = state
    }

    class ViewHolder(private var binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(viewModel: BaseViewModel,position: Int){
            binding.setVariable(BR.viewModel,viewModel)
            binding.setVariable(BR.position,position)
            binding.executePendingBindings()
        }
    }

    fun loadData(size : Int){
        this.mSize = size
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        if(viewType == NORMAL_STATE){
            val binding =
                    DataBindingUtil.inflate<ViewDataBinding>(layoutInflater, R.layout.normal_state_item, parent, false)
            return ViewHolder(binding)
        }else{
            val binding =
                    DataBindingUtil.inflate<ViewDataBinding>(layoutInflater, R.layout.search_state_item, parent, false)
            return ViewHolder(binding)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (mState is State.Normal){
            NORMAL_STATE
        }else{
            SEARCH_STATE
        }
    }

    override fun getItemCount(): Int {
        return mSize
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(viewModel,position)
    }
}
