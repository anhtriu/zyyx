package com.ssmedia.mecung.database.dao

import androidx.room.*
import com.ssmedia.mecung.database.entity.UserEntity

@Dao
interface UserDao {

    @Query("SELECT * FROM user")
    fun getAll(): List<UserEntity>

    @Query("DELETE FROM user")
    fun deleteAll()

    @Query("SELECT * FROM user WHERE id = (:userId)")
    fun getById(userId: Int): UserEntity?

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(userId: UserEntity)

    @Query("DELETE FROM user WHERE phone = (:phone)")
    fun delete(phone : String)
}