package com.zyyx.android.utils

import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.zyyx.android.R
import com.zyyx.android.extension.isPassDupOrShort


object BindingAdapter {

    @JvmStatic
    @BindingAdapter("bind:imageUrl")
    fun ImageView.setImageUrl(url: String?) {
        if (url.isNullOrEmpty()) {
            setImageResource(R.drawable.img_no_image)
            return
        }
        val requestOption = RequestOptions()
                .placeholder(R.drawable.img_no_image)
                .error(R.drawable.img_no_image).override(width, height)
        Glide.with(context).load(url).apply(requestOption).into(this)
    }

    @JvmStatic
    @BindingAdapter("username", "password", requireAll = true)
    fun View.setLoginEnabled(userName : String, password : String) {
        if(userName.isEmpty() || password.isEmpty()){
            return
        }
        isEnabled = userName == Constant.USERNAME && !password.isPassDupOrShort()
    }

}
