package com.zyyx.android.extension

fun String.isPassDupOrShort(): Boolean {
    val length = this.length
    val passwordSet = mutableSetOf<Char>()
    for (c in this) {
        passwordSet.add(c)
    }
    return length != passwordSet.size || length < 6
}