package com.zyyx.android.screen.login

import android.content.Intent
import com.ssmedia.mecung.utils.preferences.PreferencesHelper
import com.zyyx.android.R
import com.zyyx.android.base.BaseActivity
import com.zyyx.android.databinding.ActivityLoginBinding
import com.zyyx.android.screen.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.ext.android.inject

class LoginActivity : BaseActivity<ActivityLoginBinding>() {
    private val viewModel by inject<LoginViewModel>()

    override fun setLayoutId(): Int {
        return R.layout.activity_login
    }

    override fun initView() {
        if(preferencesHelper.getBoolean(PreferencesHelper.USER_LOGGED_IN)){
            startActivity(Intent(this,MainActivity::class.java))
            finish()
        }
    }

    override fun initViewModel() {
        (binding as ActivityLoginBinding).viewModel = viewModel
    }

    override fun initData() {
    }

    override fun initListener() {
        btLogin.setOnClickListener {
            viewModel.saveUser(edtUser.text.toString(),edtPass.text.toString())
            preferencesHelper.saveBoolean(PreferencesHelper.USER_LOGGED_IN,true)
            startActivity(Intent(this,MainActivity::class.java))
        }
    }

}