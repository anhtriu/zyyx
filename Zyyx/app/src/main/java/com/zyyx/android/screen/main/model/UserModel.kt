package com.zyyx.android.screen.main.model

import android.os.Parcel
import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.ssmedia.mecung.database.entity.UserEntity
import com.zyyx.android.utils.State

data class UserModel(var title: String = "",
                     var lastName: String = "",
                     var firstName: String = "",
                     var state: State?,
                     var pic: String,
                     var email: String,
                     var phone: String
                     ) : BaseObservable(), Parcelable {
    @Bindable
    var bookmark : Boolean = false
        get
        set(value) {
            field = value
            notifyPropertyChanged(BR.bookmark)
        }

    constructor(parcel: Parcel) : this(
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readString()!!,
            null,
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readString()!!) {
    }

    fun toEntity() : UserEntity{
        return UserEntity(title= title, first = firstName, last = lastName, email = email, phone = phone, pic = pic)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(lastName)
        parcel.writeString(firstName)
        parcel.writeString(pic)
        parcel.writeString(email)
        parcel.writeString(phone)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserModel> {
        override fun createFromParcel(parcel: Parcel): UserModel {
            return UserModel(parcel)
        }

        override fun newArray(size: Int): Array<UserModel?> {
            return arrayOfNulls(size)
        }
    }
}