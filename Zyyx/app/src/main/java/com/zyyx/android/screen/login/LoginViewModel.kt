package com.zyyx.android.screen.login

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.ssmedia.mecung.base.BaseViewModel
import com.ssmedia.mecung.utils.preferences.PreferencesHelper
import com.zyyx.android.BR

class LoginViewModel(val pref :PreferencesHelper) : BaseViewModel() {
    fun saveUser(userName: String, userPass: String) {
        pref.saveString(PreferencesHelper.USER_NAME,userName)
        pref.saveString(PreferencesHelper.USER_PASS,userPass)
    }

    var loginModel = LoginModel()

    class LoginModel : BaseObservable() {
        @Bindable
        var userName = ""
            get
            set(value) {
                field = value
                notifyPropertyChanged(BR.userName)
            }
        @Bindable
        var password = ""
            get
            set(value) {
                field = value
                notifyPropertyChanged(BR.password)
            }
    }

}

