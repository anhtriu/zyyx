package com.zyyx.android.screen.main

import androidx.databinding.BaseObservable
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import com.ssmedia.mecung.base.BaseViewModel
import com.ssmedia.mecung.database.dao.UserDao
import com.ssmedia.mecung.database.entity.UserEntity
import com.ssmedia.mecung.utils.preferences.PreferencesHelper
import com.zyyx.android.network.model.response.ApiResponse
import com.zyyx.android.network.service.AppApiService
import com.zyyx.android.screen.main.adapter.UserAdapter
import com.zyyx.android.screen.main.model.UserModel
import com.zyyx.android.utils.State
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.mapLatest


class MainViewModel(private val apiService: AppApiService, var userDao: UserDao, var preferencesHelper: PreferencesHelper) : BaseViewModel() {
    val userData = apiService.apiResponse
    val searching = ObservableField<Boolean>()
    private var mListUser = ArrayList<CustomMutableLiveData<UserModel>>()
    val mUserAdapter = UserAdapter(this)

    fun getData() {
        apiService.getData(1, 10, "nam")
    }

    fun getAllBookmark() {
        var bookmarkUser = userDao.getAll()
        if (bookmarkUser.isEmpty()) {
            return
        }
        io.reactivex.rxjava3.core.Observable.just(bookmarkUser).flatMap { it ->
            io.reactivex.rxjava3.core.Observable.fromIterable(it).map {
                var userModel = UserModel(it.title,
                        it.last, it.first, State.Normal(), it.pic, it.email, it.phone)
                val userLiveData = CustomMutableLiveData<UserModel>(userDao)
                userLiveData.postValue(userModel)
                userLiveData
            }.toList().toObservable()
        }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    mListUser.clear()
                    mListUser.addAll(it)
                    mUserAdapter.setState(State.Normal())
                    mUserAdapter.loadData(mListUser.size)
                }
    }

    fun setData(listItem: List<CustomMutableLiveData<UserModel>>, state: State) {
        mListUser.clear()
        mListUser.addAll(listItem)
        mUserAdapter.setState(state)
        mUserAdapter.loadData(mListUser.size)
    }


    fun getItemAt(position: Int) = mListUser[position]

    fun process(body: ApiResponse?) {
        var userList = mutableListOf<CustomMutableLiveData<UserModel>>()
        body?.let {
            body.results?.let {
                val state: State = State.Searching()
                for (resultsItem in it) {
                    resultsItem?.let { user ->
                        val user = UserModel(user.name?.title!!, user.name.last!!, user.name.first!!, state, user.picture!!.large!!, user.email!!, user.phone!!)
                        val userLiveData = CustomMutableLiveData<UserModel>(userDao)
                        userLiveData.setValue(user)
                        userList.add(userLiveData)
                    }
                }
                setData(userList, state)
            }
        }
    }

    var mUserSelected = MutableLiveData<UserModel>()
    fun onItemClicked(position: Int) {
        var user = getItemAt(position).value
        if (user?.state is State.Normal) {
            mUserSelected.value = user
        }
    }

    fun clearAll() {
        preferencesHelper.clear()
        userDao.deleteAll()
    }

    @ExperimentalCoroutinesApi
    internal val queryChannel = BroadcastChannel<String>(Channel.CONFLATED)

    @FlowPreview
    @ExperimentalCoroutinesApi
    internal val internalSearchResult = queryChannel
            .asFlow()
            .debounce(350)
            .mapLatest {
                try {
                    if(it.isNotEmpty())
                        getData()
                } catch (e: Throwable) {
                    throw e
                }
            }
            .catch { it: Throwable -> it.printStackTrace() }

    @ExperimentalCoroutinesApi
    @FlowPreview
    val searchResult = internalSearchResult.asLiveData()

    class CustomMutableLiveData<T : BaseObservable?>(userDao: UserDao) : MutableLiveData<T>() {
        override fun setValue(value: T) {
            super.setValue(value)

            //listen to property changes
            value!!.addOnPropertyChangedCallback(callback)
        }

        var callback: Observable.OnPropertyChangedCallback = object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                setValue(value!!)
                val user = value as UserModel
                if (user.bookmark) {
                    userDao.insert(user.toEntity())
                } else {
                    userDao.delete(user.phone)
                }

            }
        }
    }
}
