package com.zyyx.android.screen.detail

import com.zyyx.android.R
import com.zyyx.android.base.BaseActivity
import com.zyyx.android.databinding.ActivityDetailBinding
import com.zyyx.android.databinding.ActivityLoginBinding
import com.zyyx.android.screen.main.model.UserModel
import com.zyyx.android.utils.Constant
import kotlinx.android.synthetic.main.activity_detail.*
import org.koin.android.ext.android.inject

class DetailActivity : BaseActivity<ActivityDetailBinding>() {
    private val viewModel by inject<DetailViewModel>()

    override fun setLayoutId(): Int {
        return R.layout.activity_detail
    }

    override fun initView() {
    }

    override fun initViewModel() {
        (binding as ActivityDetailBinding).viewModel = viewModel

    }

    override fun initData() {
        var user = intent.getParcelableExtra<UserModel>(Constant.USER_PARCEL)
        viewModel.setData(user)
    }

    override fun initListener() {
        btnBack.setOnClickListener {
            finish()
        }
    }

}