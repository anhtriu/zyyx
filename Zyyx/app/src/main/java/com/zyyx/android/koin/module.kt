package com.zyyx.android.koin

import androidx.room.Room
import com.google.gson.GsonBuilder
import com.ssmedia.mecung.database.MyDatabase
import com.ssmedia.mecung.network.interceptor.ApiInterceptor
import com.ssmedia.mecung.utils.preferences.PreferencesHelper
import com.zyyx.android.network.service.ApiService
import com.zyyx.android.network.service.ApiServiceImpl
import com.zyyx.android.network.service.AppApiService
import com.zyyx.android.screen.detail.DetailViewModel
import com.zyyx.android.screen.login.LoginViewModel
import com.zyyx.android.screen.main.MainViewModel
import com.zyyx.android.utils.Constant
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {
    single {
        createWebService<ApiService>(
                createOkHttpClient(ApiInterceptor()),
                Constant.API_BASE_URL
        )
    }
}
val storageModule = module {
    single {
        Room.databaseBuilder(androidApplication(), MyDatabase::class.java, "Zyyx_DB")
                .allowMainThreadQueries()
                .build()
    }
    single { PreferencesHelper(get()) }
}

val dataModule = module {

    single { get<MyDatabase>().userDao() }
    single { ApiServiceImpl(get()) }
    factory { AppApiService(get()) }
    viewModel { LoginViewModel(get()) }
    viewModel { MainViewModel(get(),get(),get()) }
    viewModel { DetailViewModel() }
}

fun createOkHttpClient(interceptor: Interceptor): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    val timeout = 30L

    return OkHttpClient.Builder()
            .connectTimeout(timeout, TimeUnit.SECONDS)
            .readTimeout(timeout, TimeUnit.SECONDS)
            .addInterceptor(httpLoggingInterceptor)
            .addInterceptor(interceptor)
            .build()
}

inline fun <reified T> createWebService(okHttpClient: OkHttpClient, url: String): T {
    val gsonBuilder = GsonBuilder()
            .setLenient()
            .create()

    val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gsonBuilder))
            .build()

    return retrofit.create(T::class.java)
}

val myAppModule = listOf(
        networkModule,
        storageModule,
        dataModule
)