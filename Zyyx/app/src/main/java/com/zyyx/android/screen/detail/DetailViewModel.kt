package com.zyyx.android.screen.detail

import androidx.lifecycle.MutableLiveData
import com.ssmedia.mecung.base.BaseViewModel
import com.zyyx.android.screen.main.model.UserModel

class DetailViewModel : BaseViewModel(){
    val mUserModel = MutableLiveData<UserModel>()

    fun setData(userModel: UserModel?){
        userModel?.let {
            mUserModel.value = userModel
        }
    }

}