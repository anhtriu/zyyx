package com.ssmedia.mecung.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class UserEntity(
        @PrimaryKey(autoGenerate = true) val id: Int = 0,
        @ColumnInfo(name = "title") val title: String,
        @ColumnInfo(name = "first") val first: String,
        @ColumnInfo(name = "last") val last: String,
        @ColumnInfo(name = "email") val email: String,
        @ColumnInfo(name = "phone") val phone: String,
        @ColumnInfo(name = "pic") val pic: String
){
}