package com.ssmedia.mecung.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ssmedia.mecung.database.dao.UserDao
import com.ssmedia.mecung.database.entity.UserEntity

@Database(entities = [UserEntity::class], version = 1)
abstract class MyDatabase : RoomDatabase() {
    abstract fun userDao() : UserDao
}