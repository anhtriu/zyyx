package com.zyyx.android.utils

object Constant {
    const val USER_PARCEL = "USER_PARCEL"
    const val API_BASE_URL = "https://randomuser.me/"
    const val USERNAME = "usertest"
}
sealed class State{
    class Normal : State()
    class Searching : State()
}