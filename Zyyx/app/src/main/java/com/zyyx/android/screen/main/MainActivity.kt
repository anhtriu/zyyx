package com.zyyx.android.screen.main

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.zyyx.android.R
import com.zyyx.android.base.BaseActivity
import com.zyyx.android.databinding.ActivityMainBinding
import com.zyyx.android.screen.detail.DetailActivity
import com.zyyx.android.screen.login.LoginActivity
import com.zyyx.android.utils.Constant
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class MainActivity : BaseActivity<ActivityMainBinding>() {
    val TAG = "MainActivity"
    private val viewModel by inject<MainViewModel>()

    override fun setLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun initView() {
    }

    override fun initViewModel() {
        (binding as ActivityMainBinding).viewModel = viewModel
    }

    override fun initData() {
        viewModel.getAllBookmark()
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun initListener() {
        viewModel.userData.observe(this, Observer {
            Log.e(TAG, "data change")
            if (it.isSuccessful) {
                viewModel.process(it.body())
            }
        })

        cancleBt.setOnClickListener {
            viewModel.getAllBookmark()
            searchView.setQuery("",false)
            searchView.setIconified(true);
            searchView.clearFocus()
        }

        btLogout.setOnClickListener {
            viewModel.clearAll()
            finish()
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
        viewModel.searchResult.observe(this, Observer {
            Log.e(TAG, "Search Querring")
        })
        viewModel.mUserSelected.observe(this, Observer {
            val intent = Intent(this, DetailActivity::class.java)
            intent.putExtra(Constant.USER_PARCEL, it)
            startActivity(intent)
        })

        searchView.setOnQueryTextFocusChangeListener { view: View, focus: Boolean ->
            viewModel.searching.set(focus)
            if(!focus){
                viewModel.getAllBookmark()
            }
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                Log.e(TAG, "Search Querring finish")
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                lifecycleScope.launch {
                    viewModel.queryChannel.send(p0!!)
                }
                return false
            }

        })
    }

}