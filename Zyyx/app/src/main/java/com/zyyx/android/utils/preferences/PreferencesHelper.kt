package com.ssmedia.mecung.utils.preferences

import android.content.Context
import android.content.SharedPreferences

class PreferencesHelper constructor(context: Context) {
    private val sharedPref: SharedPreferences

    companion object {
        const val PREF_NAME = "PreferencePref"

        const val USER_NAME = "USER_NAME_INFO"
        const val USER_PASS = "USER_PASS_INFO"
        const val USER_LOGGED_IN = "USER_LOGGED_IN"
    }

    init {
        sharedPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }

    fun clear() {
        sharedPref.edit().clear().apply()
    }

    fun getBoolean(key: String): Boolean = sharedPref.getBoolean(key, false)

    fun saveBoolean(key: String, value: Boolean) {
        val editor = sharedPref.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun saveString(key: String, value: String) {
        val editor = sharedPref.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun getString(key: String): String = sharedPref.getString(key, "") ?: ""

}