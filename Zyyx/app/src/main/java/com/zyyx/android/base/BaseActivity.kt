package com.zyyx.android.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.ssmedia.mecung.utils.preferences.PreferencesHelper
import org.koin.android.ext.android.inject


abstract class BaseActivity<T : ViewDataBinding?> : AppCompatActivity() {
    lateinit var binding: ViewDataBinding
    protected val preferencesHelper by inject<PreferencesHelper>()

    override fun onCreate(savedInstanceState: Bundle?) {
        if (checkLogin()) {
            return
        }
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, setLayoutId())
        initView()
        initViewModel()
        initData()
        initListener()
    }



    open fun checkLogin() : Boolean {
        return false
    }

    abstract fun setLayoutId(): Int

    abstract fun initView()

    abstract fun initViewModel()

    abstract fun initData()

    abstract fun initListener()

}