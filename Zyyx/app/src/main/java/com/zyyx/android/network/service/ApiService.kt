package com.zyyx.android.network.service

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
//    https://randomuser.me/api/?page=1&results=10&seed=nam
    @GET("api/")
    suspend fun getData(@Query("page") page : Int, @Query("results") result: Int, @Query("seed") seed : String) : Response<com.zyyx.android.network.model.response.ApiResponse>
}