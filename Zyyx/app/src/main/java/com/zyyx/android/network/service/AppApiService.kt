package com.zyyx.android.network.service

import androidx.lifecycle.MutableLiveData
import com.zyyx.android.network.model.response.ApiResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

class AppApiService(private val apiService: ApiService){
    val apiResponse  = MutableLiveData<retrofit2.Response<ApiResponse>>()
    fun getData(page : Int, result: Int, seed : String) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val data = apiService.getData(page, result, seed)
                apiResponse.postValue(data)
            }catch (ex : Exception){
                apiResponse.postValue(null)
                ex.printStackTrace()
            }
        }
    }
}